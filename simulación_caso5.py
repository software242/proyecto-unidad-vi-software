#Implementación de libreria

import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint


t0=0; #tiempo inicial
tf=60; #tiempo final
ts=0.001; #tamaño del paso
t=np.arange(t0, tf+ts, ts); #Intervalo de tiempo;


h0=2; #nivel estanque en tiempo .
r=1.7841; #Radio del estanque en metros.
A=np.pi*r**2; #Área del estanque.
g=9.8;# Constante de gravedad.


qi=1; #Flujo de entrada en m^3/min
K=2; #Flujo de salida en m^3/min


t=np.arange(t0, tf+ts, ts); #Intervalo de tiempo;
def deposito(h,t):
    dhdt=(1/A)*(qi-K*np.sqrt(h))
    return dhdt
y=odeint(deposito,h0,t); #Resolver la ODE


plt.plot(t,y)
plt.xlabel("tiempo[segundos]")
plt.ylabel("Nivel de agua [metros]")

plt.show()
